#!/usr/bin/env bash
# -*- Mode: Shell Script; tab-width: 4; indent-tabs-mode: on; coding: utf-8 -*-
# Time Machine status widget
#
# Copyright(C) 2020, Michael Maier.
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

function stop() {
    echo
    
    # Restore caret.
    tput cnorm
    
    exit 0
}

trap stop SIGHUP SIGINT SIGTERM

# Extracts value from property list.
#
# $1  Property list to extract value from.
# $2  Key of property list item to extract value for. Only non-escaped
#     keys are supported.
function read_plist() {
    echo "$1" |
        grep "^[[:space:]]*$2[[:space:]]*=" |
        sed "s/^[[:space:]]*$2[[:space:]]*=[[:space:]]*\\(.*\\);\$/\\1/" |
        sed "s/^\"\\(.*\\)\"\$/\\1/"
}

# Formats percentage value from a given fraction.
function format_precentage() {
    local fraction="$1"
    
    if ! [[ "$fraction" =~ ^1|[0-9]?\.[0-9]+$ ]];
    then
        fraction=0
    fi
    
    # Note that scale is not read by multiplication, but by the division operator. Hence divide by 1.
    local percent=$( echo "scale=2; ($fraction * 100) / 1" | bc )
    
    # Add leading zero if missing.
    if test "${percent::1}" = ".";
    then
        percent="0${percent}"
    fi
    
    echo "${percent}%"
}

# Formats a given data size in bytes up to TiB units.
function format_size() {
    local size="$1"
    local scale=2
    local unit=bytes
    local factor
    
    if ! [[ "$size" =~ ^[0-9]+$ ]];
    then
        size=0
    fi

    if test "$size" -gt 1099511627776;
    then
        unit=TiB
        factor=1099511627776
    else
        if test "$size" -gt 1073741824;
        then
            unit=GiB
            factor=1073741824
        else
            if test "$size" -gt 1048576;
            then
                unit=MiB
                factor=1048576
            else
                if test "$size" -gt 1024;
                then
                    unit=KiB
                    factor=1024
                else
                    factor=1
                fi
            fi
        fi
    fi
    
    size=$( echo "scale=$scale; $size / $factor" | bc )
    
    echo "$size $unit"
}

# Formats time given in seconds.
function format_time() {
    local duration="$1"
    
    if ! [[ "$duration" =~ ^[0-9]+$ ]];
    then
        duration=0
    fi
    
    local hours=$(( $duration / 3600 ))
    local minutes=$(( ($duration - $hours * 3600) / 60 ))
    local seconds=$(( $duration % 60 ))
    
    printf "%d:%02d:%02d" $hours $minutes $seconds
}

# Hide caret.
tput civis

# Status loop.
step=0

function get_progress() {
    local stepping=
    local status=$( tmutil status )
    
    if test $? -ne 0;
    then
        echo "error: failed to read Time Machine status"
        exit 1
    fi
    
    local line="Time Machine is"
    local running=$( read_plist "$status" Running )
    
    if test "$running" = "1";
    then
        local phase=$( read_plist "$status" BackupPhase )
        local destination=$( read_plist "$status" DestinationMountPoint )
        local percent=$( read_plist "$status" Percent )
        local withDestination="on"
        
        if test "$( read_plist "$status" Stopping )" = "1";
        then
            phase=Stopping
        fi
        
        case "$phase" in
        Starting)
            phase="starting"
            withDestination="using backup volume"
            ;;
        Stopping)
            phase="stopping"
            withDestination="for backup volume"
            # Avoid progress report of previous copying phase.
            percent=-1
            ;;
        MountingBackupVol)
            phase="mounting the backup volume"
            ;;
        ThinningPreBackup)
            phase="pre-thinning"
            withDestination=
            ;;
        ThinningPostBackup)
            phase="cleaning up (post-thinning)"
            withDestination=
            ;;
        DeletingOldBackups)
            phase="deleting old backups"
            withDestination="from"
            ;;
        Copying)
            local files=$( read_plist "$status" files )
            local totalFiles=$( read_plist "$status" totalFiles )
            
            phase="copying $totalFiles files ($(( $totalFiles - $files )) in queue)"
            withDestination="to"
            ;;
        HealthCheckFsck)
            phase="verifying backup image (fsck)"
            withDestination=
            ;;
        *)
            phase="at phase '$phase'"
            ;;
        esac

        line="$line $phase"

        if test "$destination";
        then
            if test "$withDestination";
            then
                line="$line $withDestination '$destination'"
            else
                line="$line '$destination'"
            fi
        fi
        
        if [[ "$percent" =~ ^[0-9]+(\.[0-9]+)?$ ]];
        then
            line="$line: $( format_precentage $percent )"
            
            # Add progress based on bytes.
            local bytes=$( read_plist "$status" bytes )
            local totalBytes=$( read_plist "$status" totalBytes )
            
            if test "$bytes" -a "$totalBytes";
            then
                local size=$( format_size "$bytes" )
                local totalSize=$( format_size "$totalBytes" )
                
                line="$line, $size of $totalSize"
            fi
            
            # Add remainder, if estimate is present.
            local remainder=$( read_plist "$status" TimeRemaining )
            
            if [[ "$remainder" =~ ^[0-9]+$ ]];
            then
                local timeLeft=$( format_time $remainder )
                
                line="$line, ETA $timeLeft"
            fi
        fi
        
        stepping=1
    else
        if echo "$status" | grep -q "Stopping = 1;";
        then
            line="$line stopping"
            stepping=1
        else
            line="$line not active"
        fi
    fi
    
    if test "$stepping" = "1";
    then
        if test $step -gt 0;
        then
            line="$line$( printf '.%.0s' $( seq 1 $step ) )"
        fi
        
        if test $step -eq 3;
        then
            step=0
        else
            step=$(( $step + 1))
        fi
    else
        step=0
    fi
    
    echo "$line"
    
    return $step
}

while true;
do
    line=$( get_progress )
    step=$?
    
    # Clear current terminal line and write new progress line.
    echo -ne "\r\033[0K${line}"
    sleep 1
done

stop